import java.io.*;
import java.util.*;

public class BottomUpHierarchicalClustering {
	public static void main(String[] args) throws FileNotFoundException{
		ArrayList<ArrayList<Double>> data=getData();
		ArrayList<Tree> clusters=new ArrayList<Tree>();
		for(int i=0;i<data.size();i++)
			clusters.add(new Tree(data.get(i)));
		while(clusters.size()>1)
			clusters=mergeClusters(clusters);
		System.out.println(clusters.get(0));
	}
	//Calculates the distance between two points in data
	public static ArrayList<Tree> mergeClusters(ArrayList<Tree> data){
		ArrayList<Tree> clusters=new ArrayList<Tree>();
		while(data.size()>0){
			double minDist=distance(data.get(0).getX(),data.get(1).getX());
			int lowPoint1=0;
			int lowPoint2=1;
			for(int i=0;i<data.size();i++){
				for(int j=i+1;j<data.size();j++){
					double dist=distance(data.get(i).getX(),data.get(j).getX());
					if(dist<minDist){
						minDist=dist;
						lowPoint1=i;
						lowPoint2=j;
					}
				}
			}
			Tree point1=data.get(lowPoint1);
			Tree point2=data.get(lowPoint2);
			clusters.add(new Tree(centroid(point1.getX(),point2.getX()),point1,point2));
			data.remove(lowPoint2);
			data.remove(lowPoint1);
			if(data.size()==1){
				clusters.add(data.get(0));
				data.remove(0);
			}
		}
		return clusters;
	}
	public static double distance(int a, int b, ArrayList<ArrayList<Double>> data){
		double x1=data.get(a).get(0);
		double y1=data.get(a).get(1);
		double x2=data.get(b).get(0);
		double y2=data.get(b).get(1);
		return distance(x1,y1,x2,y2);
	}
	public static double distance(ArrayList<Double> a, ArrayList<Double> b){
		return distance(a.get(0),a.get(1),b.get(0),b.get(1));
	}
	public static double distance(double x1, double y1, double x2, double y2){
		return Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
	}
	public static ArrayList<Double> centroid(ArrayList<ArrayList<Double>> data){
		double sumX=0;
		double sumY=0;
		for(int i=0;i<data.size();i++){
			sumX+=data.get(i).get(0);
			sumY+=data.get(i).get(1);
		}
		ArrayList<Double> ans=new ArrayList<Double>();
		ans.add(sumX/data.size());
		ans.add(sumY/data.size());
		return ans;
	}
	public static ArrayList<Double> centroid(ArrayList<Double> x,ArrayList<Double> y){
		ArrayList<ArrayList<Double>> points=new ArrayList<ArrayList<Double>>();
		points.add(x);
		points.add(y);
		return centroid(points);
	}
	//IN DATA DELETED LINE 67 because it did not have a y coordinate
	//Stores all data from file into ArrayList of tuples called data
	public static ArrayList<ArrayList<Double>> getData() throws FileNotFoundException{
		Scanner sc=new Scanner(new File("Data.txt"));
		ArrayList<ArrayList<Double>> data=new ArrayList<ArrayList<Double>>();
		while(sc.hasNext()){
			double country=sc.nextDouble();
			double gdp=sc.nextDouble();
			ArrayList<Double> info=new ArrayList<Double>();
			info.add(country);
			info.add(gdp);
			data.add(info);
		}
		//for(int i=0;i<data.size();i++)
		//	System.out.println(data.get(i).get(1));
		sc.close();
		return data;
	}
}