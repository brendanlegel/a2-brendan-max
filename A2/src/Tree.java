import java.util.*;

public class Tree {
	private ArrayList<Double> x;
	private Tree left;
	private Tree right;
	public Tree(ArrayList<Double> x){
		this.x=x;
	}
	public Tree(ArrayList<Double> x,Tree left,Tree right){
		this.x=x;
		this.left=left;
		this.right=right;
	}
	public void setX(ArrayList<Double> x){
		this.x=x;
	}
	public void setLeft(Tree tr){
		left=tr;
	}
	public void setRight(Tree tr){
		right=tr;
	}
	public ArrayList<Double> getX(){
		return x;
	}
	public Tree getLeft(){
		return left;
	}
	public Tree getRight(){
		return right;
	}
	public String toString(){
		return "("+x+","+left+","+right+")";
	}
}
