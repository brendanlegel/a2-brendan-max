import java.io.*;
import java.util.*;

public class kmeans {
	public static final int k=3;
	public static void main(String[] args) throws FileNotFoundException{
		ArrayList<ArrayList<Double>> data=getData();
		int[] kvals=new int[k];
		ArrayList<ArrayList<ArrayList<Double>>> groupLists=new ArrayList<ArrayList<ArrayList<Double>>>();
		ArrayList<ArrayList<Double>> centroids=new ArrayList<ArrayList<Double>>();
		for(int i=0;i<k;i++){
			kvals[i]=i;
			groupLists.add(new ArrayList<ArrayList<Double>>());
			centroids.add(data.get(i));
		}
		ArrayList<ArrayList<Double>> oldCentroids=new ArrayList<ArrayList<Double>>();
		for(int i=0;i<k;i++)
			oldCentroids.add(new ArrayList<Double>());
		int x=0;
		while(!centroids.equals(oldCentroids) && x<1000){
			x++;
			for(int i=0;i<k;i++)
				oldCentroids.set(i,centroids.get(i));
			ArrayList<ArrayList<ArrayList<Double>>> newGroupLists=new ArrayList<ArrayList<ArrayList<Double>>>();
			for(int i=0;i<k;i++)
				newGroupLists.add(new ArrayList<ArrayList<Double>>());
			for(int i=0;i<k;i++)
				groupLists.set(i,new ArrayList<ArrayList<Double>>());
			for(int i=0;i<data.size();i++){
				ArrayList<Double> point=data.get(i);
				groupLists.get(whichCentroid(point,centroids)).add(point);
			}
			for(int i=0;i<k;i++)
				centroids.set(i,centroid(groupLists.get(i)));
		}
		System.out.println(x);
		for(int i=0;i<groupLists.size();i++){ //Print groupLists
			System.out.println("Group: "+i);
			ArrayList<ArrayList<Double>> group=groupLists.get(i);
			System.out.println("X coords:");
			for(int j=0;j<group.size();j++)
				System.out.println(group.get(j).get(0));
			System.out.println("Y coords:");
			for(int j=0;j<group.size();j++)
				System.out.println(group.get(j).get(1));
		}
		System.out.println("Printing Over");
	}
	//Calculates the distance between two points in data
	public static double distance(int a, int b, ArrayList<ArrayList<Double>> data){
		double x1=data.get(a).get(0);
		double y1=data.get(a).get(1);
		double x2=data.get(b).get(0);
		double y2=data.get(b).get(1);
		return distance(x1,y1,x2,y2);
	}
	public static double distance(ArrayList<Double> a, ArrayList<Double> b){
		return distance(a.get(0),a.get(1),b.get(0),b.get(1));
	}
	public static double distance(double x1, double y1, double x2, double y2){
		return Math.sqrt(Math.pow(x2-x1,2)+Math.pow(y2-y1,2));
	}
	public static ArrayList<Double> centroid(ArrayList<ArrayList<Double>> data){
		double sumX=0;
		double sumY=0;
		for(int i=0;i<data.size();i++){
			sumX+=data.get(i).get(0);
			sumY+=data.get(i).get(1);
		}
		ArrayList<Double> ans=new ArrayList<Double>();
		ans.add(sumX/data.size());
		ans.add(sumY/data.size());
		return ans;
	}
	public static int whichCentroid(ArrayList<Double> point, ArrayList<ArrayList<Double>> centroids){
		int lowIndex=0;
		double lowDist=distance(point,centroids.get(0));
		for(int i=1;i<k;i++){
			double dist=distance(point,centroids.get(i));
			if(dist<lowDist){
				lowDist=dist;
				lowIndex=i;
			}
		}
		return lowIndex;
	}
	//IN DATA DELETED LINE 67 because it did not have a y coordinate
	//Stores all data from file into ArrayList of tuples called data
	public static ArrayList<ArrayList<Double>> getData() throws FileNotFoundException{
		Scanner sc=new Scanner(new File("Data.txt"));
		ArrayList<ArrayList<Double>> data=new ArrayList<ArrayList<Double>>();
		while(sc.hasNext()){
			double country=sc.nextDouble();
			double gdp=sc.nextDouble();
			ArrayList<Double> info=new ArrayList<Double>();
			info.add(country);
			info.add(gdp);
			data.add(info);
		}
		//for(int i=0;i<data.size();i++)
		//	System.out.println(data.get(i).get(1));
		sc.close();
		return data;
	}
}